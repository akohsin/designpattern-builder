package example;

public class Main {
    public static void main(String[] args) {
        GameCharacter.Builder builder = new GameCharacter.Builder().setHealth(100).setMana(50).setName("Wrozka");
        GameCharacter numer1 = builder.create();
        GameCharacter numer3 = builder.create();
        GameCharacter numer2 = builder.create();
        builder.setHealth(10);
        GameCharacter numer4 = builder.create();
    }
}
