package ex1;

import java.util.ArrayList;
import java.util.List;

public class Client {
    private String name;
    private List<Mail> skrzynka = new ArrayList<Mail>();

    public Client(String name) {
        this.name = name;
    }

    public void readMail(Mail m){
        skrzynka.add(m);
        System.out.println("Klient " + this.getName() + " otrzymal maila");
    }

    public String getName() {
        return name;
    }
    //    Stwórz klasę Client która:
//
//            - posiada pole name
//- posiada pole List<Mail> - lista/skrzynka wiadomości klienta
//- readMail(Mail m) - która powoduje dodanie wiadomości do skrzynki i wypisanie
//    komunikatu
//
}
