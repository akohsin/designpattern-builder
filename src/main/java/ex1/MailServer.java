package ex1;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

public class MailServer {

    private List<Client> listaKlientow = new ArrayList<Client>();

    public void connect(Client c) {
        listaKlientow.add(c);
    }

    public void disconnect(Client c) {
        listaKlientow.remove(c);
    }

    public void sendMessage(Mail m, Client sender) {
        for (Client c : listaKlientow) {
            m.setDataOdbioru(LocalDateTime.now().toString());
            if (!c.equals(sender)) c.readMail(m);
        }
    }
//
//- connect(Client c) - powoduje podłączenie klienta do serwera, czyli dodanie go do
//    listy ( tej która jest polem klasy)
//- disconnect(Client c) - powoduje odłączenie klienta
//- sendMessage(Mail m, Client sender) - powoduje rozesłanie wiadomości do
//    wszystkich klientów oprócz nadawcy. Przed przekazaniem wiadomości należy ustawić jej
//    datę odebrania na teraz (now()).
}
