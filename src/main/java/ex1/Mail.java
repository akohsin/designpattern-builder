package ex1;

public class Mail {
    public void setDataOdbioru(String dataOdbioru) {
        this.dataOdbioru = dataOdbioru;
    }

    private String tresc;
    private String nadawca;
    private String dataNadania;
    private String dataOdbioru;
    private String ipNadania;
    private String ipOdebrania;
    private String nazwaSerweraPosredniego;
    private String nazwaSkrzynkiPocztowej;
    private String nazwaProtokolu;
    private TYPE typWiadomosci;
    private boolean czySzyfrowana;
    private boolean czySpam;

    public Mail(String tresc, String nadawca, String dataNadania, String dataOdbioru, String ipNadania, String ipOdebrania,
                String nazwaSerweraPosredniego, String nazwaSkrzynkiPocztowej, String nazwaProtokolu, TYPE typWiadomosci,
                boolean czySzyfrowana, boolean czySpam) {
        this.tresc = tresc;
        this.nadawca = nadawca;
        this.dataNadania = dataNadania;
        this.dataOdbioru = dataOdbioru;
        this.ipNadania = ipNadania;
        this.ipOdebrania = ipOdebrania;
        this.nazwaSerweraPosredniego = nazwaSerweraPosredniego;
        this.nazwaSkrzynkiPocztowej = nazwaSkrzynkiPocztowej;
        this.nazwaProtokolu = nazwaProtokolu;
        this.typWiadomosci = typWiadomosci;
        this.czySzyfrowana = czySzyfrowana;
        this.czySpam = czySpam;
    }
    public static class Builder{
        private String tresc;
        private String nadawca;
        private String dataNadania;
        private String dataOdbioru;
        private String ipNadania;
        private String ipOdebrania;
        private String nazwaSerweraPosredniego;
        private String nazwaSkrzynkiPocztowej;
        private String nazwaProtokolu;
        private TYPE typWiadomosci;
        private boolean czySzyfrowana;
        private boolean czySpam;

        public Builder setTresc(String tresc) {
            this.tresc = tresc;
            return this;
        }

        public Builder setNadawca(String nadawca) {
            this.nadawca = nadawca;
            return this;
        }

        public Builder setDataNadania(String dataNadania) {
            this.dataNadania = dataNadania;
            return this;
        }

        public Builder setDataOdbioru(String dataOdbioru) {
            this.dataOdbioru = dataOdbioru;
            return this;
        }

        public Builder setIpNadania(String ipNadania) {
            this.ipNadania = ipNadania;
            return this;
        }

        public Builder setIpOdebrania(String ipOdebrania) {
            this.ipOdebrania = ipOdebrania;
            return this;
        }

        public Builder setNazwaSerweraPosredniego(String nazwaSerweraPosredniego) {
            this.nazwaSerweraPosredniego = nazwaSerweraPosredniego;
            return this;
        }

        public Builder setNazwaSkrzynkiPocztowej(String nazwaSkrzynkiPocztowej) {
            this.nazwaSkrzynkiPocztowej = nazwaSkrzynkiPocztowej;
            return this;
        }

        public Builder setNazwaProtokolu(String nazwaProtokolu) {
            this.nazwaProtokolu = nazwaProtokolu;
            return this;
        }

        public Builder setTypWiadomosci(TYPE typWiadomosci) {
            this.typWiadomosci = typWiadomosci;
            return this;
        }

        public Builder setCzySzyfrowana(boolean czySzyfrowana) {
            this.czySzyfrowana = czySzyfrowana;
            return this;
        }

        public Builder setCzySpam(boolean czySpam) {
            this.czySpam = czySpam;
            return this;
        }

        public Mail createMail() {
            return new Mail(tresc, nadawca, dataNadania, dataOdbioru, ipNadania, ipOdebrania, nazwaSerweraPosredniego,
                    nazwaSkrzynkiPocztowej, nazwaProtokolu, typWiadomosci, czySzyfrowana, czySpam);
        }
    }
}
