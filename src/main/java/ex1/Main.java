package ex1;

public class Main {
    public static void main(String[] args) {
        MailServer server = new MailServer();
        Client jarek = new Client("Jarek");
        Client darek = new Client("Darek");
        Client marek = new Client("Marek");
        Client arek = new Client("Arek");
        server.connect(jarek);
        server.connect(darek);
        server.connect(marek);
        server.connect(arek);
        server.sendMessage(new Mail.Builder().setNadawca("ja").createMail(), arek);
    }
}
